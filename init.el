(setq package--init-file-ensured t)

(package-initialize)

(setq package-archives
            '(
;;("gnu" . "http://elpa.gnu.org/packages/")
              ("melpa-stable" . "http://stable.melpa.org/packages/")
              ("melpa" . "https://melpa.org/packages/")
              ))

(unless package-archive-contents
    (package-refresh-contents))

(unless (package-installed-p 'use-package)
    (package-install 'use-package))

(setq use-package-verbose t
            use-package-always-ensure t)

(eval-when-compile
    (require 'use-package))
(setq load-prefer-newer t)


;; setup  helm
(use-package helm ;pin "melpa"
  :bind (("M-x" . helm-M-x)
         ("M-<f5>" . helm-find-files)
         ([f10] . helm-buffers-list)
         ([S-f10] . helm-recentf))
    :init (progn
          (use-package helm-config :pin "melpa" :ensure t)
          (use-package helm-misc  :pin "melpa" :ensure t)
          (use-package helm-projectile :pin "melpa" :ensure t )
          (use-package helm-mode :pin "melpa" :ensure t)
          (use-package helm-match-plugin :pin "melpa" :ensure t)
          (use-package helm-buffers :pin "melpa" :ensure t)
          (use-package helm-files :pin "melpa" :ensure t)
          (use-package helm-locate  :pin "melpa" :ensure t)
          (use-package helm-bookmark :pin "melpa" :ensure t)

          (setq helm-quick-update t
                helm-bookmark-show-location t
                helm-buffers-fuzzy-matching t
                helm-input-idle-delay 0.01)

          (defun malb/helm-omni (&rest arg) 
            ;; just in case someone decides to pass an argument, helm-omni won't fail.
            (interactive)
            (helm-other-buffer
             (append ;; projectile errors out if you're not in a project 
              (if (projectile-project-p) ;; so look before you leap
                  '(helm-source-projectile-buffers-list
                    helm-c-source-buffers-list)
                '(helm-c-source-buffers-list)) ;; list of all open buffers

              (if (projectile-project-p)
                  '(helm-source-projectile-recentf-list
                    helm-c-source-recentf)
                '(helm-c-source-recentf)) ;; all recent files

              (if (projectile-project-p)
                  '(helm-source-projectile-files-list
                    helm-c-source-files-in-current-dir)
                '(helm-c-source-files-in-current-dir)) ;; files in current directory

              '(helm-c-source-locate               ;; file anywhere
                helm-c-source-bookmarks            ;; bookmarks too
                helm-c-source-buffer-not-found     ;; ask to create a buffer otherwise
                )) "*helm-omni*"))

          (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebihnd tab to do persistent action
          (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
          (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z
          )
  )
(use-package helm-swoop :pin "melpa"
  :bind (("C-c C-SPC" . helm-swoop)
         ("C-c o" . helm-multi-swoop-all)
         ("C-s"   . helm-swoop)
         ("C-r"   . helm-resume)))
(use-package helm-gtags :pin "melpa" :ensure t)

(use-package helm-make :pin "melpa" :ensure t)

(use-package helm-ispell :pin "melpa" :ensure t)

(use-package srefactor :pin "melpa" :ensure t)

(use-package lsp-java :pin "melpa-stable" :ensure t
  :init
  (add-hook 'java-mode-hook #'lsp)
  )


(use-package auto-compile
	       :init
	         (setq auto-compile-display-buffer nil
		               auto-compile-mode-line-counter t)

		   (auto-compile-on-load-mode))
(use-package s)
(use-package dash :config (dash-enable-font-lock))

(use-package diminish
  :ensure t
  :pin "melpa-stable")

(use-package cider
  :ensure t
  :pin "melpa-stable"
 :commands (cider cider-connect cider-jack-in)
  :init
  (setq cider-auto-select-error-buffer t
        cider-repl-pop-to-buffer-on-connect nil
        cider-repl-use-clojure-font-lock t
        cider-repl-wrap-history t
        cider-repl-history-size 1000
        cider-repl-history-file ".cider-history"
        cider-show-error-buffer t
        nrepl-hide-special-buffers t
        nrepl-popup-stacktraces nil)
  ;; legacy function now useless
  ;;(add-hook 'cider-mode-hook 'cider-turn-on-eldoc-mode)
  (add-hook 'cider-repl-mode-hook 'rainbow-delimiters-mode)
  ;;(add-hook 'cider-repl-mode-hook 'smartparens-strict-mode)
  (add-hook 'cider-repl-mode-hook 'subword-mode)
  (add-hook 'cider-test-report-mode 'jcf-soft-wrap)
  ;; nrepl isn't based on comint
  (add-hook 'cider-repl-mode-hook
            (lambda () (setq show-trailing-whitespace nil)))
  )

(use-package clojure-mode
  :ensure t
  :pin "melpa-stable"
  :mode (("\\.edn$" . clojure-mode))
  :config
  (progn
    (define-clojure-indent
      (defroutes 'defun)
      (GET 2)
      (POST 2)
      (PUT 2)
      (DELETE 2)
      (HEAD 2)
      (ANY 2)
      (context 2)
      (let-routes 1))

    (define-clojure-indent
      (form-to 1))

    (define-clojure-indent
      (match 1)
      (are 2)
      (checking 2)
      (async 1))

    (define-clojure-indent
      (select 1)
      (insert 1)
      (update 1)
      (delete 1))

    (define-clojure-indent
      (run* 1)
      (fresh 1))

    (define-clojure-indent
      (extend-freeze 2)
      (extend-thaw 1))

    (define-clojure-indent
      (go-loop 1))

    (define-clojure-indent
      (this-as 1)
      (specify 1)
      (specify! 1))

    (setq clojure--prettify-symbols-alist
          '(("fn" . ?λ)))

    (defun toggle-nrepl-buffer ()
      "Toggle the nREPL REPL on and off"
      (interactive)
      (if (string-match "cider-repl" (buffer-name (current-buffer)))
          (delete-window)
        (cider-switch-to-repl-buffer)))

    (defun cider-save-and-refresh ()
      (interactive)
      (save-buffer)
      (call-interactively 'cider-refresh))
    (global-set-key (kbd "s-r") 'cider-save-and-refresh))

  )

(use-package clojure-snippets
  :ensure t
  :pin "melpa-stable")


(use-package clojure-mode-extra-font-locking
  :ensure t
  :pin "melpa-stable")

(use-package flycheck
  :ensure t
  :pin "melpa-stable")


(use-package company
  :ensure t
  :pin "melpa-stable"
 :diminish company-mode
  :ensure t
  :init
  (add-hook 'after-init-hook 'global-company-mode)
  ;;(global-company-mode 1)

  :config
  (progn
    (setq company-backends
          '(company-ycmd
            company-bbdb
            company-nxml
            company-css
            company-eclim
            company-semantic
            company-xcode
            ;; company-ropemacs
            company-cmake
            company-capf
            (company-dabbrev-code company-gtags company-etags company-keywords)
            company-oddmuse
            company-files
            company-dabbrev)))

  ;; Use Helm to complete suggestions
  (define-key company-mode-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-:") 'helm-company)
  (define-key company-active-map (kbd "C-n") 'company-select-next)
  (define-key company-active-map (kbd "C-p") 'company-select-previous)
  )

(use-package company-irony
  :ensure t
  :pin "melpa-stable")
(use-package company-irony-c-headers
  :ensure t
  :pin "melpa-stable"
:init
  (add-to-list 'company-backends 'company-c-headers)
  )

(use-package company-emacs-eclim :ensure t  :pin "melpa-stable")

(use-package company-ansible :ensure t :pin "melpa-stable")

(use-package lsp-mode :commands lsp :ensure t)
(use-package lsp-ui :commands lsp-ui-mode :ensure t)
(use-package company-lsp
  :ensure t
  :commands company-lsp
  :config (push 'company-lsp company-backends)) ;; add company-lsp as a backend


(use-package company-shell :ensure t :pin "melpa-stable")

(use-package company-eldoc
  :ensure t
  :pin "melpa-stable")

(use-package company-go
  :ensure t
  :pin "melpa-stable")

(use-package go-eldoc
  :ensure t
  :pin "melpa-stable")

(use-package go-guru
  :ensure t
  :pin "melpa-stable")

(use-package gotest
  :ensure t
  :pin "melpa-stable")

(use-package company-erlang
  :ensure t
  :pin "melpa-stable")

(use-package flycheck-clojure
  :ensure t
  :pin "melpa-stable")

(use-package flycheck-irony
  :ensure t
  :pin "melpa-stable"
  :config
  (add-hook 'flycheck-mode-hook #'flycheck-irony-setup)
  )

(use-package flycheck-color-mode-line
  :ensure t
  :pin "melpa-stable")

(use-package flymake-check
  :ensure t
  :pin "melpa-stable")

;; (use-package clj-refactor
;;   :ensure t
;;   :pin "melpa-stable")


(use-package better-defaults
  :ensure t
  :pin "melpa-stable")

(use-package better-shell
  :ensure t
  :pin "melpa-stable")

(use-package adoc-mode
  :ensure t
  :pin "melpa-stable")

(use-package alect-themes
  :ensure t
  :pin "melpa-stable")

(use-package pandoc
  :ensure t
  :pin "melpa-stable")

(use-package pandoc-mode
  :ensure t
  :pin "melpa-stable")

(use-package docker
  :ensure t
  :pin "melpa-stable")

(use-package docker-compose-mode
  :ensure t
  :pin "melpa-stable")

;; (use-package cmake-ide
;;   :ensure t
;;   :pin "melpa-stable")

(use-package cmake-mode
  :ensure t
  :pin "melpa-stable")

(use-package cmake-project
  :ensure t
  :pin "melpa-stable")



(use-package ansible
  :ensure t
  :pin "melpa-stable")

(use-package ansible-doc
  :ensure t
  :pin "melpa-stable")


;; (use-package graphviz-dot-mode
;;   :ensure t
;;   :pin "melpa-stable")

(use-package "darcula-theme"
  :ensure t
  :pin "melpa-stable")

(use-package bind-key
  :ensure t
  :pin "melpa-stable")

(use-package which-key
  :ensure t
  :pin "melpa-stable"
 :config
  (setq which-key-idle-delay 1.0)
  (setq which-key-max-description-length 27)
  (setq which-key-add-column-padding 0)
  (setq which-key-max-display-columns nil)
  (setq which-key-separator " → " )
  (setq which-key-unicode-correction 3)
  (setq which-key-prefix-prefix "+" )
  (setq which-key-special-keys nil)
  (setq which-key-show-prefix 'left)
  (setq which-key-show-remaining-keys nil)
  )


(use-package ag :ensure t)

(use-package whitespace
  :ensure t
  :pin "melpa-stable"
  :diminish whitespace-mode
  :init
  (add-hook 'prog-mode-hook 'whitespace-mode)
  )

(use-package cc-mode
  :config
  (progn
    (add-hook 'c-mode-hook (lambda () (c-set-style "bsd")))
    (add-hook 'java-mode-hook (lambda () (c-set-style "bsd")))
    (setq tab-width 2)
    (setq c-basic-offset 2)))

(use-package subword
  :diminish subword-mode
  :init
  (global-subword-mode)
  )

(use-package multiple-cursors
  :ensure t
  :pin "melpa-stable"
  :bind ("C-S-C C-S-C" . mc/edit-lines)
  )


(use-package zenburn-theme :ensure t)
(use-package solarized-theme :ensure t
 :pin "melpa-stable"
 :init
  (progn
    (setq solarized-use-less-bold t
          solarized-use-more-italic t
          solarized-emphasize-indicators nil
          solarized-distinct-fringe-background nil
          solarized-high-contrast-mode-line nil))
  :config
  (progn
    (load "solarized-theme-autoloads" nil t)
    (setq theme-dark 'solarized-dark
          theme-bright 'solarized-light))
  )

(use-package smart-mode-line :ensure t
  :pin "melpa-stable"
  :config
  (sml/apply-theme 'dark)
  :init
  (progn
    (setq sml/no-confirm-load-theme t)
    (sml/setup)))

(use-package sr-speedbar :ensure t)
;; MAGIT stuff
(use-package magit
  :ensure t
  :pin "melpa-stable"
  :diminish auto-revert-mode
  :commands (magit-status magit-checkout)
  :bind (("C-x g" . magit-status))
  :init
  (setq magit-revert-buffers 'silent
        magit-push-always-verify nil
        git-commit-summary-max-length 70))


;; PROJECTILE 
(use-package projectile
  :ensure t
  :pin "melpa-stable"
  :commands (projectile-find-file projectile-switch-project)
  :diminish projectile-mode
  :init
  (use-package helm-projectile
    :ensure t
    :pin "melpa-stable"
    :bind (("s-p" . helm-projectile-find-file)
           ("s-P" . helm-projectile-switch-project)))
  :config
  (projectile-global-mode))


(use-package git-timemachine :ensure t)

(use-package eshell
  :commands eshell
  :ensure t
  :pin "melpa-stable"
  :init
  (setq
   eshell-buffer-shorthand t
   eshell-cmpl-ignore-case t
   eshell-cmpl-cycle-completions nil
   eshell-history-size 10000
   eshell-hist-ignoredups t
   eshell-error-if-no-glob t
   eshell-glob-case-insensitive t
   eshell-scroll-to-bottom-on-input 'all)
  :config
  (defun jcf-eshell-here ()
    (interactive)
    (eshell "here"))

  (defun pcomplete/sudo ()
    (let ((prec (pcomplete-arg 'last -1)))
      (cond ((string= "sudo" prec)
             (while (pcomplete-here*
                     (funcall pcomplete-command-completion-function)
                     (pcomplete-arg 'last) t))))))

  (add-hook 'eshell-mode-hook
            (lambda ()
              (define-key eshell-mode-map
                [remap eshell-pcomplete]
                'helm-esh-pcomplete)
              (define-key eshell-mode-map
                (kbd "M-p")
                'helm-eshell-history)
              (eshell/export "NODE_NO_READLINE=1"))))



(use-package recentf
  :init
  (recentf-mode 1)
  :pin "melpa-stable"
  :ensure t
  :config
  (setq
   recentf-max-saved-items 1000
   recentf-exclude '("/tmp/" "/ssh:")))

(use-package paredit
  :ensure t
  :pin "melpa-stable"
  :diminish paredit-mode
  :init
  (add-hook 'clojure-mode-hook 'enable-paredit-mode)
  (add-hook 'cider-repl-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'emacs-lisp-mode-hook 'enable-paredit-mode)
  (add-hook 'lisp-interaction-mode-hook 'enable-paredit-mode)
  (add-hook 'ielm-mode-hook 'enable-paredit-mode)
  (add-hook 'json-mode-hook 'enable-paredit-mode))

(use-package paren-face :ensure t
  :pin "melpa-stable"
  :init
  (global-paren-face-mode)
  :config
  (add-hook 'clojure-mode-hook (lambda () (setq paren-face-regexp "#?[](){}[]"))))



;; (use-package auctex
;;   :ensure t
;;   :mode ("\\.tex\\'" . latex-mode)
;;   :commands (latex-mode LaTeX-mode plain-tex-mode)
;;   :init
;;   (progn
;;     (add-hook 'LaTeX-mode-hook #'LaTeX-preview-setup)
;;     (add-hook 'LaTeX-mode-hook 'LaTeX-math-mode)
;;     (add-hook 'LaTeX-mode-hook #'flyspell-mode)
;;     (add-hook 'LaTeX-mode-hook #'turn-on-reftex)
;;     (setq TeX-auto-save t
;;           TeX-parse-self t
;;           TeX-save-query nil
;;           TeX-PDF-mode t)
;;     ))

;; ;; Use company-auctex
;; (use-package company-auctex
;;   :ensure t
;;   :config
;;   (company-auctex-init)
;;   )

(use-package web-mode
  :ensure t
  :pin "melpa-stable"
  :config
  (progn
    ;; Enable web mode in the following modes
    (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.js?\\'" . web-mode))
    (add-to-list 'auto-mode-alist '("\\.jsx?\\'" . web-mode))
    ;; Set the content type to "jsx" for the following file extensions
    (setq web-mode-content-types-alist
          '(("jsx" . "\\.js[x]?\\'")))
    )
  )


(use-package rainbow-delimiters :ensure t
  :pin "melpa-stable"
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(use-package markdown-mode :ensure t :pin "melpa-stable"
  :mode (("\\.markdown$" . markdown-mode)
         ("\\.md$" . markdown-mode)))


(use-package spaceline :ensure t :pin "melpa-stable")

(use-package yasnippet
  :ensure t
  :pin "melpa-stable"
  :init
  (yas-global-mode 1))



(use-package adoc-mode :ensure t)

(use-package git-timemachine :ensure t :pin "melpa-stable")

(use-package gitconfig :ensure t :pin "melpa-stable")

(use-package gitconfig-mode :ensure t :pin "melpa-stable")

(use-package cyberpunk-theme :ensure t :pin "melpa-stable")

;;(use-package anti-zenburn-theme :ensure t :pin "melpa")

(use-package clang-format :ensure t :pin "melpa")

(use-package cmake-font-lock :ensure t :pin "melpa")

(use-package cmake-ide :ensure t :pin "melpa")

(use-package undo-propose :ensure t :pin "melpa")

(defun my-org-confirm-babel-evaluate (lang body)
  (not (member lang '("C" "clojure" "sh" "java" "plantuml" "ditaa"))))

(setq org-confirm-babel-evaluate 'my-org-confirm-babel-evaluate)

;; 	 (ensime . "melpa-stable")
	 

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(company-quickhelp-color-background "#b0b0b0")
 '(company-quickhelp-color-foreground "#232333")
 '(compilation-message-face (quote default))
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#839496")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   (quote
    ("3c83b3676d796422704082049fc38b6966bcad960f896669dfc21a7a37a748fa" "0598c6a29e13e7112cfbc2f523e31927ab7dce56ebb2016b567e1eff6dc1fd4f" "73c69e346ec1cb3d1508c2447f6518a6e582851792a8c0e57a22d6b9948071b4" "c74e83f8aa4c78a121b52146eadb792c9facc5b1f02c917e3dbb454fca931223" "84890723510d225c45aaff941a7e201606a48b973f0121cb9bcb0b9399be8cba" "8eafb06bf98f69bfb86f0bfcbe773b44b465d234d4b95ed7fa882c99d365ebfd" default)))
 '(highlight-changes-colors (quote ("#d33682" "#6c71c4")))
 '(highlight-symbol-colors
   (--map
    (solarized-color-blend it "#002b36" 0.25)
    (quote
     ("#b58900" "#2aa198" "#dc322f" "#6c71c4" "#859900" "#cb4b16" "#268bd2"))))
 '(highlight-symbol-foreground-color "#93a1a1")
 '(highlight-tail-colors
   (quote
    (("#073642" . 0)
     ("#546E00" . 20)
     ("#00736F" . 30)
     ("#00629D" . 50)
     ("#7B6000" . 60)
     ("#8B2C02" . 70)
     ("#93115C" . 85)
     ("#073642" . 100))))
 '(hl-bg-colors
   (quote
    ("#7B6000" "#8B2C02" "#990A1B" "#93115C" "#3F4D91" "#00629D" "#00736F" "#546E00")))
 '(hl-fg-colors
   (quote
    ("#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36" "#002b36")))
 '(hl-paren-colors (quote ("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900")))
 '(magit-diff-use-overlays nil)
 '(nrepl-message-colors
   (quote
    ("#dc322f" "#cb4b16" "#b58900" "#546E00" "#B4C342" "#00629D" "#2aa198" "#d33682" "#6c71c4")))
 '(package-selected-packages (quote (dash s auto-compile use-package)))
 '(pdf-view-midnight-colors (quote ("#232333" . "#c7c7c7")))
 '(pos-tip-background-color "#073642")
 '(pos-tip-foreground-color "#93a1a1")
 '(show-paren-mode t)
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#073642" 0.2))
 '(term-default-bg-color "#002b36")
 '(term-default-fg-color "#839496")
 '(tool-bar-mode nil)
 '(vc-annotate-background nil)
 '(vc-annotate-background-mode nil)
 '(vc-annotate-color-map
   (quote
    ((20 . "#dc322f")
     (40 . "#c8805d801780")
     (60 . "#bec073400bc0")
     (80 . "#b58900")
     (100 . "#a5008e550000")
     (120 . "#9d0091000000")
     (140 . "#950093aa0000")
     (160 . "#8d0096550000")
     (180 . "#859900")
     (200 . "#66aa9baa32aa")
     (220 . "#57809d004c00")
     (240 . "#48559e556555")
     (260 . "#392a9faa7eaa")
     (280 . "#2aa198")
     (300 . "#28669833af33")
     (320 . "#279993ccbacc")
     (340 . "#26cc8f66c666")
     (360 . "#268bd2"))))
 '(vc-annotate-very-old-color nil)
 '(weechat-color-list
   (quote
    (unspecified "#002b36" "#073642" "#990A1B" "#dc322f" "#546E00" "#859900" "#7B6000" "#b58900" "#00629D" "#268bd2" "#93115C" "#d33682" "#00736F" "#2aa198" "#839496" "#657b83")))
 '(xterm-color-names
   ["#073642" "#dc322f" "#859900" "#b58900" "#268bd2" "#d33682" "#2aa198" "#eee8d5"])
 '(xterm-color-names-bright
   ["#002b36" "#cb4b16" "#586e75" "#657b83" "#839496" "#6c71c4" "#93a1a1" "#fdf6e3"]))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
